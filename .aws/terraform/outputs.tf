output "gateway_base_url" {
    value = aws_apigatewayv2_stage.default.invoke_url
}

output "cognito_user_pool_id" {
    value = aws_cognito_user_pool.default.id
}

output "cognito_user_pool_client_id" {
    value = aws_cognito_user_pool_client.default.id
}

output "cloudfront_default_domain" {
    value = module.website.cloudfront_default_domain
}

output "cloudfront_id" {
    value = module.website.cloudfront_id
}

output "default_s3_bucket_name" {
    value = module.website.bucket_name
}

output "digital_assets_s3_bucket_name" {
    value = module.images_bucket.aws_s3_bucket.bucket
}

output "aws_region" {
    value = data.aws_region.current.name
}