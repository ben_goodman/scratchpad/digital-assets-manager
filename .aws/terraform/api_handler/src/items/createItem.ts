import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, PutCommand } from "@aws-sdk/lib-dynamodb";
import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3"
import { error } from "console"
import { customAlphabet } from 'nanoid'

const s3 = new S3Client({})
const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME
const IMAGES_BUCKET_NAME = process.env.IMAGES_BUCKET_NAME

const b64toBlob = (base64, type = 'application/octet-stream') =>
  fetch(`data:${type};base64,${base64}`).then(res => res.blob())


export const createItem = async (body?: string) => {
    if (body === undefined) {
        throw new Error('Body undefined.')
    }

    try {
        const requestJSON = JSON.parse(body)

        const nanoid = customAlphabet('1234567890abcdefghijklmnopqrstuvwxyz', 14)
        const id = nanoid()

        console.log('converting image data to array buffer')
        const image = await b64toBlob(requestJSON.image)
        const objectKey = `assets/${id}/${requestJSON.title}`
        const imageBuf = Buffer.from(await image.arrayBuffer())

        await s3.send(
            new PutObjectCommand({
                Bucket: IMAGES_BUCKET_NAME,
                Key: objectKey,
                Body: imageBuf,
                ContentType: requestJSON.type,
            })
        ).catch((err) => {
            console.log(`uploading image for item ${id} failed`)
            console.log(err)
            throw err
        })

        await dynamo.send(
            new PutCommand({
                TableName: TABLE_NAME,
                Item: {
                    id,
                    title: requestJSON.title,
                    description: requestJSON.description,
                    objectKey: objectKey,
                    type: requestJSON.type,
                    dimensions: requestJSON.dimensions,
                    dateCreated: new Date().toISOString()
                },
            })
        ).catch((err) => {
            console.log(`adding item ${id} to dynamo failed`)
            console.log(err)
            throw err
        })

        return id
    } catch(err){
        console.log(`put item failed`)
        throw error
    }
}
