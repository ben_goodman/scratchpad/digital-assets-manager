import { DynamoDBDocumentClient, GetCommand, type GetCommandOutput } from '@aws-sdk/lib-dynamodb'
import { DynamoDBClient } from '@aws-sdk/client-dynamodb'

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export const getItem = async (key: string): Promise<GetCommandOutput> => {
    return dynamo.send(
        new GetCommand({
            TableName: TABLE_NAME,
            Key: {
                id: key,
            },
        })
    )
}
