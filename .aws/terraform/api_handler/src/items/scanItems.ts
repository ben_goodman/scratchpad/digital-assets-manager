import { DynamoDBClient, ScanCommand, type ScanCommandOutput } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient } from "@aws-sdk/lib-dynamodb";

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export interface ScanItemsOptions {
    limit?: string
    from?: string
}

export const scanItems = async ({
    limit,
    from
}: ScanItemsOptions = {}): Promise<ScanCommandOutput> => {
    return dynamo.send(new ScanCommand({
        ProjectionExpression: 'id, dateCreated, objectKey, title',
        TableName: TABLE_NAME,
        ExclusiveStartKey: from ? { id: { S: from } } : undefined,
        Limit: limit && parseInt(limit) || undefined
    }))
}
