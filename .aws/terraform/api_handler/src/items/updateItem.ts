import { DynamoDBClient } from "@aws-sdk/client-dynamodb"
import { DynamoDBDocumentClient, UpdateCommand } from "@aws-sdk/lib-dynamodb"

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export const updateItem = async (id: string, body?: string) => {
    if (!body) {
        throw new Error('No body provided')
    }

    const {description} = JSON.parse(body)

    if (description) {
        await dynamo.send(
            new UpdateCommand({
                TableName: TABLE_NAME,
                Key: {
                    id,
                },
                UpdateExpression: "set description = :description",
                ExpressionAttributeValues: {
                    ":description": description,
                },
            })
        ).catch((err) => {
            console.log(`updating item ${id} failed`)
            console.log(err)
            throw err
        })
    }
}