import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { DeleteObjectCommand, S3Client } from '@aws-sdk/client-s3'
import { GetCommand, DeleteCommand, DynamoDBDocumentClient, type DeleteCommandOutput } from '@aws-sdk/lib-dynamodb'

const s3 = new S3Client({})
const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME
const IMAGES_BUCKET_NAME = process.env.IMAGES_BUCKET_NAME


export const deleteItem = async (id: string): Promise<DeleteCommandOutput> => {
    // get the S3 key of the deleting item
    const itemResponse = await dynamo.send(
        new GetCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    ).catch((err) => {
        console.log(`failed to get item ${id}.`)
        console.log(err)
        throw err
    })

    console.log(itemResponse.Item)

    await s3.send(
        new DeleteObjectCommand({
            Bucket: IMAGES_BUCKET_NAME,
            Key: (itemResponse as any).Item.objectKey,
        })
    ).catch((err) => {
        console.log(`deleting image for item ${id} failed`)
        console.log(err)
        throw err
    })

    return dynamo.send(
        new DeleteCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    ).catch((err) => {
        console.log(`deleting item ${id} failed`)
        console.log(err)
        throw err
    })
}
