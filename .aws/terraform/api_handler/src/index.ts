import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { scanItems, getItem, createItem, deleteItem, updateItem } from './items'

const headers = {"Content-Type": "application/json"}

const getBody = (event: APIGatewayProxyEvent) => {
    return event.body && event.isBase64Encoded
        ? Buffer.from(event.body!, 'base64').toString('utf-8')
        : (event.body || undefined)
}

export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log('event', event)
    const  { resource, httpMethod } = event

    if (httpMethod === 'GET' && resource === '/api/health') {
        return {
            headers,
            statusCode: 200,
            body: JSON.stringify({ message: 'Ok' }),
        }
    }

    // GET /api/asset/{id} - get specific asset
    if (httpMethod === 'GET' && resource === '/api/asset/{id}') {
        const { id } = event.pathParameters as any
        const item = await getItem(id)
        return {
            headers,
            statusCode: 200,
            body: JSON.stringify(item),
        }
    }

    // GET /api/asset - list assets
    if (httpMethod === 'GET' && resource === '/api/asset') {
        const { from, limit } = event.queryStringParameters || {}
        const items = await scanItems({ from, limit })
        return {
            headers,
            statusCode: 200,
            body: JSON.stringify(items),
        }
    }

    // PUT /api/asset - upload an asset
    if (httpMethod === 'POST' && resource === '/api/asset') {
        const body = getBody(event)
        const resp = await createItem(body)
        return {
            headers,
            statusCode: 200,
            body: JSON.stringify(resp),
        }
    }

    // PATCH /api/asset - modify an asset's metadata
    if (httpMethod === 'PATCH' && resource === '/api/asset/{id}') {
        const body = getBody(event)
        const { id } = event.pathParameters as any
        try {
            await updateItem(id, body)
            return {
                headers,
                statusCode: 200,
                body: JSON.stringify({ message: 'updated' }),
            }
        } catch(error) {
            return {
                headers: {"Content-Type": "application/json"},
                statusCode: 500,
                body: JSON.stringify(error)
            }
        }
    }

    // DELETE /api/asset/{id} - delete an asset
    if (httpMethod === 'DELETE' && resource === '/api/asset/{id}') {
        try {
            const { id } = event.pathParameters as any
            const resp = await deleteItem(id)
            return {
                headers,
                statusCode: 200,
                body: JSON.stringify(resp),
            }
        } catch(error) {
            return {
                headers,
                statusCode: 500,
                body: JSON.stringify(error)
            }
        }
    }

    // default response
    return {
        headers,
        statusCode: 404,
        body: JSON.stringify({ message: 'Resource not found' }),
    }
}
