terraform {
    backend "s3" {
        bucket = "tf-states-us-east-1-bgoodman"
        key    = "digital-assets-manager/terraform.tfstate"
        region = "us-east-1"
    }
}