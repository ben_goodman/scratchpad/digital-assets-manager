module "auth_handler_payload" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws//modules/code_builder"
    version = "3.0.0"

    output_filename = "index.js"
    source_root     = "${path.module}/auth_handler"
    build_command   = "make all"
}

module "authorizer_lambda_function" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "3.0.0"

    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = module.auth_handler_payload.archive_file
    function_name    = "auth-handler-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 512
    runtime          = "nodejs18.x"

    environment_variables = {
        USER_POOL_ID  = aws_cognito_user_pool.default.id
        APP_CLIENT_ID = aws_cognito_user_pool_client.default.id
    }
}

resource "aws_lambda_permission" "allow_gateway_invoke" {
    statement_id  = "example-api-auth-gateway-exec-${random_id.cd_function_suffix.hex}"
    action        = "lambda:InvokeFunction"
    function_name = module.authorizer_lambda_function.name
    principal     = "apigateway.amazonaws.com"
    source_arn    = "${aws_apigatewayv2_api.default.execution_arn}/*/*"
}
