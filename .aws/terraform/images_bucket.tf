resource "aws_cloudfront_origin_access_identity" "images_bucket" {
    comment = "images-bucket-${var.project_name}-${var.resource_namespace}"
}

module "images_bucket" {
    source  = "gitlab.com/ben_goodman/s3-website/aws//modules/s3"
    version = "2.0.0"

    oai_arn     = aws_cloudfront_origin_access_identity.images_bucket.iam_arn
    bucket_name = "${var.project_name}-assets.${var.resource_namespace}"
    tags = {}
}