resource "aws_cognito_user_pool" "default" {
  name = "${var.project_name}-user-pool"

  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  }

  admin_create_user_config {
    allow_admin_create_user_only = true
  }
}

resource "aws_cognito_user_pool_client" "default" {
    name                                 = "${var.project_name}-pool-client"
    user_pool_id                         = aws_cognito_user_pool.default.id
    callback_urls                        = ["http://localhost:1234"]
    allowed_oauth_flows_user_pool_client = true
    allowed_oauth_flows                  = ["code", "implicit"]
    allowed_oauth_scopes                 = ["email", "openid"]
    supported_identity_providers         = ["COGNITO"]
    explicit_auth_flows                  = ["ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]

    refresh_token_validity  = 1
    access_token_validity   = 60
    id_token_validity       = 60

    token_validity_units {
        refresh_token = "days"
        access_token  = "minutes"
        id_token      = "minutes"
    }
}

resource "aws_cognito_user_pool_domain" "default" {
  domain       = "${var.project_name}-${random_id.cd_function_suffix.hex}-login"
  user_pool_id = aws_cognito_user_pool.default.id
}
