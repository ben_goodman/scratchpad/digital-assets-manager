import { type APIGatewayRequestAuthorizerEventV2 } from 'aws-lambda'
import { CognitoJwtVerifier } from 'aws-jwt-verify'

interface SimpleResponse {
    isAuthorized: boolean,
    context?: {}
}

const USER_POOL_ID = process.env.USER_POOL_ID
const APP_CLIENT_ID = process.env.APP_CLIENT_ID

export const handler = async (event: APIGatewayRequestAuthorizerEventV2): Promise<SimpleResponse> => {
    const token = event?.headers?.authorization
    console.log('token', token)
    console.log({USER_POOL_ID, APP_CLIENT_ID})

    if (USER_POOL_ID === undefined || APP_CLIENT_ID === undefined) {
        throw new Error('USER_POOL_ID and APP_CLIENT_ID must be set')
    }

    if (token) {
        try {
            const verifier = CognitoJwtVerifier.create({
                userPoolId: USER_POOL_ID,
                clientId: APP_CLIENT_ID,
                tokenUse: "access",
            })
            await verifier.verify(token)
            // allow - valid token
            return {
                'isAuthorized': true
            }
        } catch (error) {
            // deny - invalid token
            console.error(error)
            return {
                'isAuthorized': false
            }
        }
    } else {
        // deny - no token
        return {
            'isAuthorized': false
        }
    }
}
