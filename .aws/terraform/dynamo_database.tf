resource "aws_dynamodb_table" "default" {
    name             = "${var.dynamo_table_name}-${random_id.cd_function_suffix.hex}-${terraform.workspace}"
    billing_mode     = "PAY_PER_REQUEST"
    hash_key         = "id"

    // sys.id
    attribute {
        name = "id"
        type = "S"
    }
}
