# Digital Asset Manager

A basic image assets manager.

## Usage
1. Apply Terraform from `.aws/terraform`.
1. Create a new user in the Cognito user pool.
1. Validate the new user.  The user pool id is provided by the Terraform output `cognito_user_pool_id`.
    ```bash
    aws cognito-idp admin-set-user-password \
    --user-pool-id '<cognito_user_pool_id>' \
    --username 'example-user' \
    --password 'password1234' --permanent
    ```
1. The UI's hostname is provided by the Terraform output `cloudfront_default_domain`.

