import React from 'react'
import { Notification } from '@contentful/f36-notification'

import { Modal } from '@contentful/f36-modal'
import { useAsset } from 'src/services/useAsset'
import { Button } from '@contentful/f36-button'

export interface AssetDeleteButtonProps {
    assetId: string
    onSuccess?: () => void
}

export const AssetDeleteButton = ({
    assetId,
    onSuccess = () => {}
}: AssetDeleteButtonProps) => {
    const asset = useAsset()
    const [isConfirmationModalOpen, setConfirmationModalOpen] = React.useState<boolean>(false)
    const [isButtonLoading, setButtonLoading] = React.useState<boolean>(false)

    const handleAssetDeletion = async () => {
        setButtonLoading(true)
        try {
            await asset.delete(assetId)
            Notification.success('Asset deleted.')
            onSuccess?.()
        } catch (error) {
            Notification.error('Failed to delete asset.')
            console.error(error)
        }
        setButtonLoading(false)
    }

    return (
        <>
            <Modal onClose={() => setConfirmationModalOpen(false)} isShown={isConfirmationModalOpen}>
                {() => (
                    <>
                        <Modal.Header
                            title="Delete Asset"
                            onClose={() => setConfirmationModalOpen(false)}
                        />
                        <Modal.Content>
                            <>
                                <Button
                                    variant='negative'
                                    onClick={handleAssetDeletion}
                                    isLoading={isButtonLoading}
                                >
                                    Delete Asset
                                </Button>
                            </>
                        </Modal.Content>
                    </>
                )}
            </Modal>

            <Button
                variant='negative'
                onClick={() => setConfirmationModalOpen(true)}
                isLoading={isButtonLoading}
            >
                Delete Asset
            </Button>
        </>
    )
}