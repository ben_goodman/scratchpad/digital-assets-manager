import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Button } from '@contentful/f36-button'
import { Stack } from '@contentful/f36-core'
import { CopyButton } from '@contentful/f36-copybutton'
import { Form, FormControl, TextInput, Textarea } from '@contentful/f36-forms'
import { Image } from '@contentful/f36-image'
import { Notification } from '@contentful/f36-notification'
import type { AssetData } from 'src/types'
import { useAsset } from 'src/services/useAsset'
import { AssetDeleteButton } from 'src/components/AssetDeleteButton'



export interface AssetEditFormProps {
    assetData?: AssetData
}

export const AssetEditForm = ({
    assetData
}: AssetEditFormProps) => {
    const navigate = useNavigate()
    const asset = useAsset()
    const [assetDescription, setAssetDescription] = React.useState<string>('')
    const [isDescriptionUpdated, setIsDescriptionUpdated] = React.useState<boolean>(false)
    const [isButtonLoading, setButtonLoading] = React.useState<boolean>(false)

    React.useEffect(() => {
        if (assetData?.description) {
            setAssetDescription(assetData?.description)
        }
    }, [assetData?.description])

    const handleAssetDescriptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAssetDescription(e.target.value)
        setIsDescriptionUpdated(true)
    }

    const handleAssetDescriptionSave = async () => {
        setButtonLoading(true)
        try {
            await asset.update(assetData!.id, {description: assetDescription})
            setIsDescriptionUpdated(false)
            Notification.success('Asset description updated.')
        } catch(error) {
            Notification.error("Failed to update asset description.")
        }
        setButtonLoading(false)
    }

    return (
        <>
            <Form>
                <FormControl>
                    <FormControl.Label>
                        Asset Path
                    </FormControl.Label>

                    <TextInput.Group>
                        <TextInput isDisabled isReadOnly value={assetData?.objectKey} />
                        <CopyButton
                            value={assetData?.objectKey || ''}
                            tooltipProps={{ placement: 'right', usePortal: true }}
                        />
                    </TextInput.Group>

                </FormControl>

                <FormControl>
                    <FormControl.Label>
                        Description
                    </FormControl.Label>
                    <Stack
                        flexDirection='column'
                        alignItems='flex-start'
                    >
                        <Textarea
                            isDisabled={!assetData}
                            value={assetDescription}
                            onChange={handleAssetDescriptionChange}
                        />
                        <Button
                            isDisabled={!isDescriptionUpdated}
                            onClick={handleAssetDescriptionSave}
                            isLoading={isButtonLoading}
                        >
                            Save
                        </Button>
                    </Stack>
                </FormControl>

                <FormControl isReadOnly>
                    <FormControl.Label>
                        Asset
                    </FormControl.Label>
                    <Image
                        src={'/' + assetData?.objectKey}
                        alt={assetData?.description || 'loading'}
                        width='600px'
                        height='600px'
                        style={{
                            objectFit: 'scale-down'
                        }}
                    />
                </FormControl>

                <FormControl isReadOnly isDisabled >
                    <FormControl.Label>
                        Dimensions
                    </FormControl.Label>
                    <TextInput
                        type='text'
                        value={`${assetData?.dimensions[0]} x ${assetData?.dimensions[1]}`}
                    />
                </FormControl>

                <FormControl isReadOnly isDisabled>
                    <FormControl.Label>
                        Type
                    </FormControl.Label>
                    <TextInput value={assetData?.type}/>
                </FormControl>

                <AssetDeleteButton
                    assetId={assetData!.id}
                    onSuccess={() => navigate('/')}
                />

            </Form>


        </>
    )
}