import React from 'react'
import { Modal } from '@contentful/f36-modal'
import { Notification } from '@contentful/f36-notification'
import { AssetUploadForm } from 'src/components/AssetUploadForm'

export interface AssetUploadModalProps {
    state: [boolean, React.Dispatch<React.SetStateAction<boolean>>]
    onSuccess?: (id: string) => void
    onError?: (error: any) => void
}

export const AssetUploadModal = ({
    state,
    onSuccess = () => {},
    onError = () => {}
}: AssetUploadModalProps) => {
    const [isOpen, setOpen] = state
    return (
        <Modal onClose={() => setOpen(false)} isShown={isOpen}>
        {() => (
            <>
                <Modal.Header
                    title="Upload Asset"
                    onClose={() => setOpen(false)}
                />
                <Modal.Content>
                    <AssetUploadForm
                        onSuccess={(id) => {
                            setOpen(false)
                            Notification.success('Image uploaded.')
                            onSuccess?.(id)
                        }}
                        onError={(error) => {
                            setOpen(false)
                            Notification.error('Image upload failed.')
                            onError?.(error)
                        }}
                    />
                </Modal.Content>
            </>
        )}
        </Modal>
    )
}