import React, { useState, useCallback } from 'react'
import styled from 'styled-components'
import { Form, FormControl, TextInput } from '@contentful/f36-forms'
import { Button } from '@contentful/f36-button'
import { Stack } from '@contentful/f36-core'
import { Image as F36Image } from '@contentful/f36-image'

import { useAsset } from '../../services/useAsset'

const ImageInput = styled.input`
    width: 100%;
    padding: 10px;
    box-sizing: border-box;
`

export interface ImageUploadProps {
    onCancel?: () => void
    onError?: (error: unknown) => void
    onSuccess?: (id: string) => void
}

export const AssetUploadForm = ({
    onCancel,
    onError,
    onSuccess
}: ImageUploadProps) => {
    const asset = useAsset()
    const [imageFile, setImageFile] = useState<File | null>(null)
    const [imageDescription, setImageDescription] = useState<string>('')
    const [imageTitle, setImageTitle] = useState<string>('')
    const [imageDimensions, setImageDimensions] = useState<[number, number] | null>(null)
    const [isSubmitButtonDisabled, setSubmitButtonDisabled] = useState<boolean>(false)

    const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setImageTitle(e.target.value)
    }

    const handleDescriptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setImageDescription(e.target.value)
    }

    const handleImageChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            const image = e.target.files[0]

            const img = new Image()
            img.src = URL.createObjectURL(image)

            img.onload = () => {
                setImageDimensions([img.width, img.height])
            }

            setImageFile(image)
            setImageTitle(image?.name || '')
        }
    },[])

    const handleSubmit = async () => {
        if (imageFile && imageDimensions) {
            setSubmitButtonDisabled(true)
            try {
                const id = await asset.upload(
                    imageTitle,
                    imageDescription,
                    imageDimensions,
                    imageFile,
                )
                setSubmitButtonDisabled(false)
                onSuccess?.(id)
            } catch (err) {
                console.log(err)
                setSubmitButtonDisabled(false)
                onError?.(err)
            }
        }
    }

    return (
        <Form onSubmit={handleSubmit}>
            <FormControl isRequired>
                <FormControl.Label>Title</FormControl.Label>
                <TextInput
                    type="text"
                    value={imageTitle}
                    onChange={handleTitleChange}
                />
                <FormControl.HelpText>Enter the image&apos;s title.</FormControl.HelpText>
            </FormControl>

            <FormControl>
                <FormControl.Label>Description</FormControl.Label>
                <TextInput
                    type="text"
                    value={imageDescription}
                    onChange={handleDescriptionChange}
                />
                <FormControl.HelpText>Enter the image&apos;s description.</FormControl.HelpText>
            </FormControl>

            <FormControl isRequired>
                <FormControl.Label>File</FormControl.Label>
                <ImageInput
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                />
                <F36Image
                    src={imageFile && URL.createObjectURL(imageFile) || undefined}
                    alt="preview"
                    width="400px"
                    height="300px"
                />
            </FormControl>

            <Stack>
                <Button isDisabled={isSubmitButtonDisabled} variant="primary" type="submit">Submit</Button>
                {onCancel && <Button variant="negative" onClick={onCancel}>Cancel</Button>}
            </Stack>
        </Form>
    )
}