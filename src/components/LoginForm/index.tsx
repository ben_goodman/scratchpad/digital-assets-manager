import React from 'react'
import {
    Center,
    FormControl,
    FormLabel,
    Input,
    Button,
    Box,
    VStack,
    useToast,
} from '@chakra-ui/react'
import { useAuthentication } from '../../services/useAuthentication'

export interface LoginFormProps {
    onSuccess?: (token: string) => void
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    onError?: (error: any) => void
}

export const LoginForm = ({
    onSuccess = () => {},
    onError = () => {},
}: LoginFormProps) => {
    const [isLoading, setIsLoading] = React.useState(false)
    const [username, setUsername] = React.useState('')
    const [password, setPassword] = React.useState('')
    const toast = useToast()
    const { login } = useAuthentication()

    const submitLogin = () => {
        setIsLoading(true)
        login(username, password)
            // on login success
            .then((token) => {
                onSuccess?.(token)
                toast({
                    title: 'Login Successful.',
                    status: 'success',
                    duration: 2000,
                    isClosable: true,
                })
            })
            // on login fail
            .catch((error) => {
                onError?.(error)
                toast({
                    title:  'Invalid username or password.',
                    status: 'error',
                    duration: 2000,
                    isClosable: true,
                })
            })
            .finally(() => {
                setIsLoading(false)
            })
    }

    return (
        <Center>
            <Box maxW={300}>
                <VStack>
                    <FormControl isRequired>
                        <FormLabel>Username</FormLabel>
                        <Input
                            type='text'
                            name='username'
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                        />
                    </FormControl>
                    <FormControl isRequired>
                        <FormLabel>Password</FormLabel>
                        <Input
                            type='password'
                            name='password'
                            onChange={(e) => setPassword(e.target.value)}
                            value={password}
                        />
                    </FormControl>
                    <Button
                        isLoading={isLoading}
                        onClick={submitLogin}
                    >
                        Submit
                    </Button>
                </VStack>
            </Box>
        </Center>
    )
}