import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Header as F36Header } from '@contentful/f36-header'
import { Flex } from '@contentful/f36-core'
import { Button } from '@contentful/f36-button'
import tokens from '@contentful/f36-tokens'
import { AssetUploadModal } from 'src/components/AssetUploadModal'


export const Header = () => {
    const navigate = useNavigate()
    const modalState = React.useState(false)

    return (
        <>
            <F36Header
                title="View Assets"
                withBackButton={true}
                backButtonProps={{
                    onClick: () => navigate('/')
                }}
                actions={
                    <Flex
                        alignItems="center"
                        gap={tokens.spacingS}
                        justifyContent="flex-end"
                    >
                        <Button
                            onClick={() => modalState[1](true)}
                            variant='primary'
                        >
                            Upload Asset
                        </Button>
                    </Flex>
                }
            />

            <AssetUploadModal
                state={modalState}
                onSuccess={(id) => navigate(`/asset/${id}`)}
            />
        </>
    )

}
