import React from 'react'
import { useNavigate } from 'react-router-dom'
import InfiniteScroll from 'react-infinite-scroll-component'
import { Flex } from '@contentful/f36-core'
import { Spinner } from '@contentful/f36-spinner'
import { Card } from '@contentful/f36-card'
import { Image } from '@contentful/f36-image'
import { useAsset } from 'src/services/useAsset'
import type { ItemScanProjection } from 'src/types'


export const Gallery = () => {
    const navigate = useNavigate()
    const assets = useAsset()
    const [assetData, setAssetData] = React.useState<ItemScanProjection[]|undefined>(undefined)
    const [lastEvaluatedKey, setLastEvaluatedKey] = React.useState<string|undefined>(undefined)


    const fetchData = async () => {
        console.log('fetching data')
        const data = await assets.list({
            limit: 5,
            from: lastEvaluatedKey
        })
        setAssetData( [...assetData || [], ...data.Items] )
        setLastEvaluatedKey(data.LastEvaluatedKey?.id?.S)
    }

    React.useEffect(() => void fetchData(), [])


    if (!assetData) {
        return (
            <Spinner variant="default" size="large" />
        )
    }

    return (
        <InfiniteScroll
            dataLength={assetData.length}
            next={fetchData}
            hasMore={Boolean(lastEvaluatedKey)}
            loader={<Spinner variant="default" size="large" />}
        >
            <Flex
                flexWrap='wrap'
            >
                {
                    assetData.map( image => {
                        return (
                            <Card
                                onClick={() => navigate(`/asset/${image.id.S}`)}
                                style={{
                                    width: '300px',
                                    height: '300px'
                                }}
                            >
                                <Image
                                    src={image.objectKey.S}
                                    alt={image.title.S}
                                    width='300px'
                                    height='300px'
                                    style={{
                                        objectFit: 'scale-down'
                                    }}
                                />
                        </Card>
                        )
                    })
                }
            </Flex>
        </InfiniteScroll>
    )
}
