import { LoginForm } from '../components/LoginForm'
import React from 'react'
import { useNavigate } from 'react-router-dom'

export const Login = () => {
    const navigate = useNavigate()

    return (
        <>
            <LoginForm onSuccess={() => navigate('/')}/>
        </>
    )
}
