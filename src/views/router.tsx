import React from 'react'
import { Navigate, Outlet, Route, createBrowserRouter, createRoutesFromElements } from 'react-router-dom'
import { useAuthentication } from 'src/services/useAuthentication'
import { Header } from 'src/components/Header'
import { Gallery } from './Gallery'
import { Login } from './Login'
import { Error } from './Error'
import { Asset } from './Asset'


const Main = () => {
    const { isAuthenticated } = useAuthentication()
    return (
        <>
            { isAuthenticated() && <Header /> }
            <Outlet />
        </>
    )
}

interface RequireAuthProps extends React.PropsWithChildren {}

const Protected = ({ children }: RequireAuthProps) => {
    const { isAuthenticated } = useAuthentication()
    return isAuthenticated() && children || <Navigate to="/login" replace />
}

export const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path='/' element={ <Main />}>
            <Route
                index
                element={ <Protected><Gallery /></Protected> }
            />
            <Route
                path='asset/:id'
                element={ <Protected><Asset /></Protected> }
            />
            <Route
                path='login'
                element={ <Login /> }
            />
            <Route
                path='*'
                element={ <Error /> }
            />
        </Route>
    )
)