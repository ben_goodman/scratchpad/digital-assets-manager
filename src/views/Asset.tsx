import React, { useEffect } from 'react'
import { useParams } from 'react-router'
import { AssetEditForm } from 'src/components/AssetEditForm'

import { useAsset } from 'src/services/useAsset'
import { AssetData } from 'src/types'

export const Asset = () => {
    const { id } = useParams()
    const asset = useAsset()
    const [assetData, setAssetData] = React.useState<AssetData|undefined>(undefined)

    useEffect(() => {
        if (id) {
            asset.get(id).then(resp => {
                setAssetData(resp.Item)
            })
        }
    }, [id])

    return assetData && (
        <>
            <AssetEditForm assetData={assetData}/>
        </>
    )
}