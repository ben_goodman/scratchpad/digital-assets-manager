import { AuthFlowType, CognitoIdentityProvider, CognitoIdentityProviderClient, InitiateAuthCommand } from '@aws-sdk/client-cognito-identity-provider'
import { jwtDecode } from 'jwt-decode'
import { useLocalStorage } from '@rehooks/local-storage'


const requestAuthentication = async (username: string, password: string): Promise<string> => {
    if (process.env.AWS_REGION === undefined) {
        throw new Error('AWS_REGION is not defined')
    }

    if (process.env.USER_POOL_CLIENT_ID === undefined) {
        throw new Error('USER_POOL_CLIENT_ID is not defined')
    }

    const client = new CognitoIdentityProviderClient({
        region: process.env.AWS_REGION,
    })

    const command = new InitiateAuthCommand({
        AuthFlow: AuthFlowType.USER_PASSWORD_AUTH,
        AuthParameters: {
            USERNAME: username,
            PASSWORD: password,
        },
        ClientId: process.env.USER_POOL_CLIENT_ID,
    })

    const data = await client.send(command)
    const token = data.AuthenticationResult?.AccessToken
    if (!token) {
        throw new Error('No token found')
    }
    return token
}


const revokeAuthentication = async (token: string) => {
    if (process.env.AWS_REGION === undefined) {
        throw new Error('AWS_REGION is not defined')
    }

    const client = new CognitoIdentityProvider({
        region: process.env.AWS_REGION,
    })

    return client.globalSignOut({AccessToken: token})
}


const isTokenExpired = (token: string|null) => {
    if (!token) return true
    const { exp } = jwtDecode(token)
    if (!exp) return true

    const isTokenExpired = Date.now() >= exp * 1000
    return isTokenExpired
}

const SESSION_AUTH_TOKEN = 'session_auth_token'

export const useAuthentication = () => {

    const [token, setToken, deleteToken] = useLocalStorage(SESSION_AUTH_TOKEN)

    return {
        isAuthenticated(): boolean {
            return !isTokenExpired(token)
        },
        getToken(): string|undefined {
            return token || undefined
        },
        async login(username: string, password: string): Promise<string> {
            const jwt = await requestAuthentication(username, password)
            setToken(jwt)
            return jwt
        },
        async logout(): Promise<void> {
            token && await revokeAuthentication(token)
            deleteToken()
        },
    }
}
