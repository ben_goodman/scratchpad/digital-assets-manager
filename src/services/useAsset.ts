import type { PageOptions, ItemScanResp, ItemGetResp } from 'src/types'
import { useAuthentication } from 'src/services/useAuthentication'


export interface UpdateOptions {
    description?: string
}

export const useAsset = () => {
    const auth = useAuthentication()

    return {
        // Fetches a list of assets, optionally paginated.
        list: async ({from, limit}: PageOptions = {}) => {
            const url = new URL('./api/asset', window.location.origin)
            from && url.searchParams.append('from', from)
            limit && url.searchParams.append('limit', limit.toString())
            const response = await fetch(url)
            return (await response.json()) as ItemScanResp
        },

        // Uploads an asset.
        upload: async (
            title: string,
            description: string,
            dimensions: [number, number],
            imageFile: File
        ): Promise<string> => {
            const image = await imageFile.arrayBuffer()
                .then(buffer => Buffer.from(buffer).toString('base64'))

            const body = JSON.stringify({
                title,
                description,
                image,
                dimensions,
                type: imageFile.type,
            })
            const url = new URL('./api/asset', window.location.origin)
            const resp = await fetch(
                url,
                {
                    method: 'POST',
                    headers: {
                        'authorization': auth.getToken() || '',
                    },
                    body,
                }
            )
            return (await resp.json())
        },

        // Fetches a single asset by ID.
        get: async (id: string) => {
            const url = new URL(`./api/asset/${id}`, window.location.origin)
            const response = await fetch(url)
            return (await response.json()) as ItemGetResp
        },

        // Updates an asset by ID.
        update: async (id: string, opts: UpdateOptions = {}) => {
            const url = new URL(`./api/asset/${id}`, window.location.origin)
            const resp = await fetch(
                url,
                {
                    method: 'PATCH',
                    headers: {
                        'authorization': auth.getToken() || '',
                    },
                    body: JSON.stringify(opts),
                }
            )
            return (await resp.json())
        },

        // Deletes an asset by ID.
        delete: async (id: string) => {
            const url = new URL(`./api/asset/${id}`, window.location.origin)
            const resp = await fetch(
                url,
                {
                    method: 'DELETE',
                    headers: {
                        'authorization': auth.getToken() || '',
                    },
                }
            )
            return (await resp.json())
        }
    }
}