import { type ScanCommandOutput, type GetCommandOutput } from '@aws-sdk/lib-dynamodb'

export interface PageOptions {
    from?: string,
    limit?: number
}

export interface DynamoScanResponse<T> extends Omit<ScanCommandOutput, 'Items'> {
    Items: T[]
}

export interface DynamoGetResponse<T> extends Omit<GetCommandOutput, 'Item'> {
    Item: T
}

export interface DynamoString {
    S: string
}

export interface ItemScanProjection {
    id: DynamoString
    dateCreated: DynamoString
    objectKey: DynamoString
    title: DynamoString
}

export interface AssetData {
    dateCreated: string
    description: string
    dimensions: number[]
    id: string
    objectKey: string
    title: string
    type: string
}


export type ItemScanResp = DynamoScanResponse<ItemScanProjection>
export type ItemGetResp = DynamoGetResponse<AssetData>

